function SpaceObject(_name, _object_diameter,
                     _orbit_radius, _orbit_duration_days, _satelites) {
  this._name = _name;
  this._object_diameter = _object_diameter;
  this._orbit_radius = _orbit_radius;
  this._orbit_duration_days = _orbit_duration_days;
  this._theta = 0;
  this._theta_velocity = 2 * Math.PI / _orbit_duration_days;
  
  if (_satelites) {
    this._satelites = _satelites;
  } else {
    this._satelites = [];
  }
};

SpaceObject.prototype.move = function(_fps) {
  this._theta += this._theta_velocity / _fps;
  for (var i = 0; i < this._satelites.length; i++) {
    this._satelites[i].move(_fps);
  }
};

SpaceObject.prototype.getOrbitRadius = function(_scale) {
  return Math.max(5, _scale * this._orbit_radius);
};

SpaceObject.prototype.getObjectRadius = function(_scale) {
  return Math.max(2, _scale * this._object_diameter / 2.0);
};

SpaceObject.prototype.getPositionCoordinates = function(_scale, _center) {
  var center_x = _center[0];
  var center_y = _center[1];
  if (this._orbit_radius > 0) {
    var radius = this.getOrbitRadius(_scale);
    var x = radius * cos(this._theta);
    var y = radius * sin(this._theta);
    return [center_x + x, center_y + y];
  }
  return [center_x, center_y];
};

var sun = new SpaceObject("sun", 696.34, 0, 0, [
  new SpaceObject("mercury", 4.88, 69800, 88.0),
  new SpaceObject("venus", 12.10, 108900, 224.7),
  new SpaceObject("earth", 12.76, 152600, 365.25, 
    [new SpaceObject("luna", 3.474, 405, 27.0)]),
  new SpaceObject("mars", 6.79, 228000, 687.0, 
    [new SpaceObject("deimos", 0.015, 23500, 1.2), new SpaceObject("phobos", 0.027, 9380, 0.31)]),
  new SpaceObject("jupiter", 142.98, 816000, 4343.5)
]);

var level_colors = ['yellow', 'red', 'blue']

// var space_scale = 0.00025;
var space_scale = 0.00035;
function draw_space_object(_space_object, _center, _level) {
  var center_x = _center[0];
  var center_y = _center[1];
  
  var position =_space_object.getPositionCoordinates(space_scale, _center);
  var x = position[0];
  var y = position[1];

  var object_r = _space_object.getObjectRadius(space_scale);
  
  //draw orbit
  if (_space_object._orbit_radius > 0) {
    var orbit_r = _space_object.getOrbitRadius(space_scale);
    
    stroke(50, 50, 50);
    noFill();
    ellipse(center_x, center_y, orbit_r * 2, orbit_r * 2);
  }

  //draw object
  object_color = level_colors[_level];
  stroke(object_color);
  fill(object_color);
  ellipse(x, y, object_r * 2, object_r * 2);
  
  for (var i = 0; i < _space_object._satelites.length; i++) {
    draw_space_object(_space_object._satelites[i], position, _level+1)
  }
};

function setup() {
  createCanvas(1024, 768);
  frameRate(24)
}

function draw() {
  background(0);
  translate(width/2, height/2);
  ellipseMode(CENTER);

  draw_space_object(sun, [0, 0], 0);
  sun.move(2)
}
